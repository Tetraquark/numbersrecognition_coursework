# -*- coding: utf-8 -*-

# var 16, nums: 5,7,9

# rgb more than [55, 55, 55]

import cv2
import math
from collections import defaultdict
from sklearn.neural_network import MLPClassifier
from sklearn import metrics

IMG_TRAIN_PATH_FOLDER = "./data/train/"
IMG_TEST_PATH_FOLDER = "./data/test/"
IMG_PATH_PREFIX_TRAIN = "mnist_train"
IMG_PATH_PREFIX_TEST = "mnist_test"

def main_func():
    numbers_keys = [5, 7, 9]
    train_imgs, test_imgs = load_mnist_raw_imgs(numbers_keys)
    train_tiles, test_tiles = split_images_to_tiles(numbers_keys, train_imgs, test_imgs)

    hidden_layers_arr = range(2, 6)
    activate_funs_arr = ['identity', 'logistic']
    X_train_zones, Y_train_zones, X_test_zones, Y_test_zones = tiles_processing(numbers_keys, train_tiles, test_tiles, mode = "zones")
    X_train_hist, Y_train_hist, X_test_hist, Y_test_hist = tiles_processing(numbers_keys, train_tiles, test_tiles, mode = "histogram")
    
    for layer_num in hidden_layers_arr:
        for activate_fun in activate_funs_arr:
            score, neur_num = count_neuralnet(X_train_zones, Y_train_zones, X_test_zones, Y_test_zones, activate_fun, layer_num)
            print("zones, " + activate_fun + ", hid.layers num " + str(layer_num) + ", neur.num " + str(neur_num) + ": " + str(score) )
            score, neur_num = count_neuralnet(X_train_hist, Y_train_hist, X_test_hist, Y_test_hist, activate_fun, layer_num)
            print("histo, " + activate_fun + ", hid.layers num " + str(layer_num) + ", neur.num " + str(neur_num) + ": " + str(score))

    return 0

def count_neuralnet(X_train, Y_train, X_test, Y_test, activ_func, hidden_layers_num):
    hidden_neur_num = int(math.sqrt(2 * len(X_train[0])))
    
    laysers_size = ()
    for i in range(hidden_layers_num):
        laysers_size = laysers_size + (hidden_neur_num, )
    clf = MLPClassifier(solver='lbfgs', activation=activ_func, hidden_layer_sizes=laysers_size, random_state=1)
    clf.fit(X_train, Y_train)
    clf_res = clf.predict(X_test)
    
    return metrics.accuracy_score(Y_test, clf_res), hidden_neur_num

def load_mnist_raw_imgs(images_nums_list, img_format = "jpg"):
    raw_test_images = dict.fromkeys(images_nums_list)
    raw_train_images = dict.fromkeys(images_nums_list)
    
    for image_number in images_nums_list:
        raw_test_images[image_number] = cv2.imread(IMG_TEST_PATH_FOLDER + IMG_PATH_PREFIX_TEST + str(image_number) + "." + img_format)
        raw_train_images[image_number] = cv2.imread(IMG_TRAIN_PATH_FOLDER + IMG_PATH_PREFIX_TRAIN + str(image_number) + "." + img_format)   
             
    return raw_train_images, raw_test_images

def split_images_to_tiles(images_nums_list, raw_train_img_dict, raw_test_img_dict, train_tilse_num = 73, test_tiles_num = 29, tile_pixel_size = 28):
    train_tiles_dict = defaultdict(list)
    test_tiles_dict = defaultdict(list)
    
    for i in images_nums_list:
        for x in range(test_tiles_num):
            for y in range(test_tiles_num):
                tile = raw_test_img_dict[i][x * tile_pixel_size : x * tile_pixel_size + tile_pixel_size, y * tile_pixel_size : y * tile_pixel_size + tile_pixel_size]
                test_tiles_dict[i].append(tile)
        for x in range(train_tilse_num):
            for y in range(train_tilse_num):
                tile = raw_train_img_dict[i][x * tile_pixel_size : x * tile_pixel_size + tile_pixel_size, y * tile_pixel_size : y * tile_pixel_size + tile_pixel_size]
                train_tiles_dict[i].append(tile)    
    
    return train_tiles_dict, test_tiles_dict

def tile_processing_zones(tile, tile_size = 28, zoneside_pixs_num = 7):
    tileside_zones_num = tile_size / zoneside_pixs_num # 7
    processed_tile = []
    
    for i in range(tileside_zones_num):
        for j in range(tileside_zones_num):
            white_pxl_num = 0
            for x in range(zoneside_pixs_num):
                for y in range(zoneside_pixs_num):
                    rgb = tile[x + i * zoneside_pixs_num, y + j * zoneside_pixs_num]
                    if rgb[0] > 50 and rgb[1] > 50 and rgb[2] > 50:
                        white_pxl_num = white_pxl_num + 1
            white_pxl_num_norm = float(white_pxl_num) / float(zoneside_pixs_num * zoneside_pixs_num)
            processed_tile.append(white_pxl_num_norm)
    
    return processed_tile

def tile_processing_histogram(tile, tile_size = 28):
    processed_tile = []

    white_pxl_num_horz = 0
    
    for x in range(tile_size):
        white_pxl_num_vert = 0
        for y in range(tile_size):
            rgb = tile[x, y]
            if rgb[0] > 50 and rgb[1] > 50 and rgb[2] > 50:
                white_pxl_num_vert = white_pxl_num_vert + 1
                
        processed_tile.append(white_pxl_num_vert)
        
    for y in range(tile_size):
        white_pxl_num_horz = 0
        for x in range(tile_size):
            rgb = tile[x, y]
            if rgb[0] > 50 and rgb[1] > 50 and rgb[2] > 50:
                white_pxl_num_horz = white_pxl_num_horz + 1
                
        processed_tile.append(white_pxl_num_horz)    

    return processed_tile
"""
    @param mode: "zones" (default) or "histogram" 
"""
def tiles_processing(images_nums_list, train_tiles_dict, test_tiles_dict, mode = "zones", tile_size = 28, zoneside_pixs_num = 7):
    train_zones_data_dict = defaultdict(list)
    test_zones_data_dict = defaultdict(list)
    print(mode)
    X_train = []
    Y_train = []
    X_test = []
    Y_test = []
    
    for number_key in images_nums_list:
        for tile in train_tiles_dict[number_key]:
            #train_zones_data_dict[number_key].append(tile_processing(tile, tile_size, zoneside_pixs_num))
            if mode == "histogram":
                X_train.append(tile_processing_histogram(tile, tile_size))
            else:
                X_train.append(tile_processing_zones(tile, tile_size, zoneside_pixs_num))
            Y_train.append(number_key)
        for tile in test_tiles_dict[number_key]:
            #test_zones_data_dict[number_key].append(tile_processing(tile, tile_size, zoneside_pixs_num))
            if mode == "histogram":
                X_test.append(tile_processing_histogram(tile, tile_size))
            else:
                X_test.append(tile_processing_zones(tile, tile_size, zoneside_pixs_num))
            Y_test.append(number_key)
    
    print("X_train = " + str(len(X_train)) + "Y_train = " + str(len(Y_train)) + "X_test = " + str(len(X_test)) + "Y_test = " + str(len(Y_test)))           
    return X_train, Y_train, X_test, Y_test #train_zones_data_dict, test_zones_data_dict

main_func()
 